+++
title = "Información local"
weight = 2
chapter = false
+++


## Hey Rasta, ¿Cómo esta el tráfico hacia a mi trabajo? <i class="fas fa-traffic-light"></i>

## Hey Rasta, llévame al aeropuerto <i class="fas fa-plane-departure"></i>

## Hey Rasta, ¿Cuál es el pronóstico del clima para mañana? <i class="fas fa-cloud-sun-rain"></i>
