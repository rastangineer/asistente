+++
title = "Package rasta.assistant.devices"
weight = 3
chapter = false
+++

- [CreateDeviceRequest](#createdevicerequest)
- [DeleteDeviceRequest](#deletedevicerequest)

## CreateDeviceRequest

Request message for DevicesPlatformService.CreateDevice.

Fields | Description
-------|------------------------------------------------------------------------------
device | [Device](https://developers.google.com/assistant/sdk/reference/rpc/google.assistant.devices.v1alpha2#google.assistant.devices.v1alpha2.Device) . Raw device info provided by user. device_id should be unique in the project, assigned by project owner.
parent | **string** . The name of the project in which device belongs to, of the form `projects/{project_id}`

## DeleteDeviceRequest

Request message for DevicesPlatformService.DeleteDevice.

Fields | Description
-------|----------------------------------------------------------------------------------------------------------------------------------
name   | **string** . Resource name of the project in which to delete the device, of the form `projects/{project_id}/devices/{device_id}`.
