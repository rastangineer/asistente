+++
title = 'Android'
weight = 2
chapter = false
+++
# <i class="fab fa-android"></i>

![googleplay](https://play.google.com/intl/en_us/badges/images/generic/es_badge_web_generic.png?width=20pc)
![f-droid](https://f-droid.org/badge/get-it-on.png?width=20pc)
