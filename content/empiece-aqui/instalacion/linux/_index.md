+++
title = "Linux"
weight = 2
chapter = false
+++

# <i class="fab fa-linux"></i>

* Instala el paquete usando tu application manager de preferencia:

  ```bash
  $ sudo apt-get update
  $ sudo apt-get install rastasistente
  ```

* O clona el repositorio:

  ```bash
  $ git clone https://gitlab.com/rastangineer/rastasistente.git
  $ cd rastasistente
  $ ./rastasistente
  ```
