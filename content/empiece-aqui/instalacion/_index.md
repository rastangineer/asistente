+++
title = "Instalación"
weight = 2
chapter = false
+++

**RastAsistente** está disponible para todos los dispositivos y sistemas operativos.

* Android <i class="fab fa-android"></i>
* iOS <i class="fab fa-app-store-ios"></i>
* Linux <i class="fab fa-linux"></i>
* Windows <i class="fab fa-windows"></i>
* OSX <i class="fab fa-apple"></i>
