+++
title = "Configuración"
weight = 3
chapter = false
+++

Aquí empieza la documentación de la **Configuración**

{{< mermaid >}}
graph LR;
    L[Laptop <br/> fa:fa-laptop] -- commit .md --> G[Gitlab <br/> fa:fa-git] --> a
    subgraph Pipeline
    a[HTML <br/> fa:fa-code ]-->b[Ortografía <br/> fa:fa-font]-->c[Estilo <br/> fa:fa-camera-retro ]
    end
    c-->P{Error fa:fa-question-circle};
    P -->|Yes| B[Gitlab Pages <br/> fa:fa-rocket];
    P -->|No| A[Abort <br/> fa:fa-trash];
{{< /mermaid >}}

```mermaid
graph LR;
    L[Laptop <br/> fa:fa-laptop] -- commit .md --> G[Gitlab <br/> fa:fa-git] --> a
    subgraph Pipeline
    a[HTML <br/> fa:fa-code ]-->b[Ortografía <br/> fa:fa-font]-->c[Estilo <br/> fa:fa-camera-retro ]
    end
    c-->P{Error fa:fa-question-circle};
    P -->|Yes| B[Gitlab Pages <br/> fa:fa-rocket];
    P -->|No| A[Abort <br/> fa:fa-trash];
```
